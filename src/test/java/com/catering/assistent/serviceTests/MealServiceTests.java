package com.catering.assistent.serviceTests;


import com.catering.assistent.business.MealService;
import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.request.RequestPostMeal;
import com.catering.assistent.model.response.ResponseGetMeal;
import com.catering.assistent.repository.MealRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoMoreInteractions;


@RunWith(MockitoJUnitRunner.class)
public class MealServiceTests {

    @Mock
    private MealRepository mealRepository;

    @InjectMocks
    private MealService mealService;
    private Long id = 1L;
    private Meal meal = new Meal("sarme");
    private List<Meal> mealList = new ArrayList<>();
    private ResponseGetMeal responseGetMeal = new ResponseGetMeal(meal);
    private RequestPostMeal requestPostMeal = new RequestPostMeal(meal.getMealName());

    //    public List<ResponseGetMeal> returnAllMealsService();
    @Test
    public void testReturnAllMealsService() {
        //prepare
        mealList.add(meal);
        List<ResponseGetMeal> expected = new ArrayList<>();
        expected.add(responseGetMeal);
        Mockito.when(mealRepository.findAll()).thenReturn(mealList);
        //execute
        List<ResponseGetMeal> actual = mealService.returnAllMealsService();
        //assert
        assertEquals(actual.get(0).getMealName(), expected.get(0).getMealName());
    }

    //    public ResponseGetMeal returnMealById(Long id) throws MyResourceNotFoundException;
    @Test
    public void testReturnMealById() throws MyResourceNotFoundException {
        //prepare
        Mockito.when(mealRepository.findById(id)).thenReturn(Optional.of(meal));
        ResponseGetMeal expected = responseGetMeal;
        //execute
        ResponseGetMeal actual = mealService.returnMealById(id);
        //assert
        assertEquals(actual.getMealName(), expected.getMealName());
    }

    //    public ResponseGetMeal returnMealByMealName(String mealName) throws MyResourceNotFoundException;
    @Test
    public void testReturnMealByMealName() throws MyResourceNotFoundException {
        //prepare
        Mockito.when(mealRepository.findByMealName(meal.getMealName())).thenReturn(Optional.of(meal));
        ResponseGetMeal expected = responseGetMeal;
        //execute
        ResponseGetMeal actual = mealService.returnMealByMealName(meal.getMealName());
        //assert
        assertEquals(actual.getMealName(), expected.getMealName());
    }

    //    public void deleteMealById(Long id) throws MyResourceNotFoundException;
    @Test
    public void testDeleteMealById() {
        //prepare
        //execute
        mealRepository.deleteById(id);
        //assert
        Mockito.verify(mealRepository, times(1)).deleteById(id);
        verifyNoMoreInteractions(mealRepository);
    }

    //    public ResponseGetMeal saveMeal(RequestPostMeal requestPostMeal) throws DataMissingException;
    @Test
    public void testSaveMeal() throws DataMissingException {
        //prepare
        ResponseGetMeal expected = responseGetMeal;
        //execute
        ResponseGetMeal actual = mealService.saveMeal(requestPostMeal);
        //assert
        Mockito.verify(mealRepository, times(1)).save(any(Meal.class));
        verifyNoMoreInteractions(mealRepository);
        assertEquals(actual.getMealName(), expected.getMealName());

    }

    //    public ResponseGetMeal updateMeal(Long id, RequestPostMeal requestPostMeal) throws DataMissingException, MyResourceNotFoundException;
    @Test
    public void testUpdateMeal() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        ResponseGetMeal expected = responseGetMeal;
        Mockito.when(mealRepository.findById(id)).thenReturn(Optional.of(meal));
        //execute
        ResponseGetMeal actual = mealService.updateMeal(id, requestPostMeal);
        //assert
        assertEquals(actual.getMealName(), expected.getMealName());
    }
}
