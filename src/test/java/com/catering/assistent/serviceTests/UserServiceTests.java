package com.catering.assistent.serviceTests;


import com.catering.assistent.business.UserService;
import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.EmailExistentException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.User;
import com.catering.assistent.model.request.RequestPostUser;
import com.catering.assistent.model.response.ResponseGetUser;
import com.catering.assistent.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;


@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {


    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;
    private Long id = 1L;
    private User user = new User("Mile", "Kitic", "mile@kitic.com");
    private List<User> userList = new ArrayList<>();
    private ResponseGetUser responseGetUser = new ResponseGetUser(user);
    private RequestPostUser requestPostUser = new RequestPostUser(user.getFirstName(), user.getLastName(), user.getEmail());

    //     public List<ResponseGetUser> returnAllUsersService();

    @Test
    public void testReturnAllUsersService() {
        //prepare
        userList.add(user);
        List<ResponseGetUser> expected = new ArrayList<>();
        expected.add(responseGetUser);
        Mockito.when(userRepository.findAll()).thenReturn(userList);
        //execute
        List<ResponseGetUser> actual = userService.returnAllUsersService();
        //assert
        assertEquals(actual.get(0).getUserEmail(), expected.get(0).getUserEmail());
        assertEquals(actual.get(0).getUserFirstName(), expected.get(0).getUserFirstName());
        assertEquals(actual.get(0).getUserLastName(), expected.get(0).getUserLastName());
    }


    //    public ResponseGetUser returnUserById(Long id) throws MyResourceNotFoundException;

    @Test
    public void testReturnUserById() throws MyResourceNotFoundException {
        //prepare
        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));
        ResponseGetUser expected = responseGetUser;
        //execute
        ResponseGetUser actual = userService.returnUserById(id);
        //assert
        assertEquals(actual.getUserEmail(), expected.getUserEmail());
        assertEquals(actual.getUserFirstName(), expected.getUserFirstName());
        assertEquals(actual.getUserLastName(), expected.getUserLastName());

    }

    @Test
    public void testReturnUserByEmail() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
        ResponseGetUser expected = responseGetUser;
        //execute
        ResponseGetUser actual = userService.returnUserByEmail(user.getEmail());
        //assert
        assertEquals(actual.getUserEmail(), expected.getUserEmail());
        assertEquals(actual.getUserFirstName(), expected.getUserFirstName());
        assertEquals(actual.getUserLastName(), expected.getUserLastName());

    }

    @Test(expected = DataMissingException.class)
    public void testReturnUserByEmailWithoutProperEmail() throws DataMissingException, MyResourceNotFoundException {
        String crockedEmail = "crock";
        userService.returnUserByEmail(crockedEmail);
    }

    @Test(expected = MyResourceNotFoundException.class)
    public void testReturnUserByEmailWithoutEmailInDB() throws DataMissingException, MyResourceNotFoundException {
        userService.returnUserByEmail(user.getEmail());
    }

    //    public void deleteUserById(Long id) throws MyResourceNotFoundException;

    @Test
    public void testDeleteUserById() {
        //prepare
        //excute
        userRepository.deleteById(id);
        //assert
        verify(userRepository, times(1)).deleteById(id);
        verifyNoMoreInteractions(userRepository);
    }

    //    public ResponseGetUser saveUser(RequestPostUser requestPostUser) throws DataMissingException;

    @Test
    public void testSaveUser() throws DataMissingException, EmailExistentException {
        //prepare
        ResponseGetUser expected = responseGetUser;
        //execute
        ResponseGetUser actual = userService.saveUser(requestPostUser);

        //assert
        assertEquals(actual.getUserEmail(), expected.getUserEmail());
        assertEquals(actual.getUserFirstName(), expected.getUserFirstName());
        assertEquals(actual.getUserLastName(), expected.getUserLastName());

    }

    //    public ResponseGetUser updateUser(Long id, RequestPostUser requestPostUser) throws DataMissingException,MyResourceNotFoundException;
    @Test
    public void testUpdateUser() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        ResponseGetUser expected = responseGetUser;

        Mockito.when(userRepository.findById(id)).thenReturn(Optional.of(user));

        //execute
        ResponseGetUser actual = userService.updateUser(id, requestPostUser);

        //assert

        assertEquals(actual.getUserEmail(), expected.getUserEmail());
        assertEquals(actual.getUserFirstName(), expected.getUserFirstName());
        assertEquals(actual.getUserLastName(), expected.getUserLastName());

    }
}
