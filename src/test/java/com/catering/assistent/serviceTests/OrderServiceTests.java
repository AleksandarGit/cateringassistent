package com.catering.assistent.serviceTests;

import com.catering.assistent.business.OrderService;
import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.OrderFromUser;
import com.catering.assistent.model.User;
import com.catering.assistent.model.request.RequestPostOrder;
import com.catering.assistent.model.response.ResponseGetAllOrders;
import com.catering.assistent.model.response.ResponseGetAllOrdersByEmail;
import com.catering.assistent.model.response.ResponseGetOrder;
import com.catering.assistent.repository.MealRepository;
import com.catering.assistent.repository.OrderRepository;
import com.catering.assistent.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.catering.assistent.util.StringDateConverter.returnTwoAdjecentDays;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verifyNoMoreInteractions;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTests {
    @Mock
    private OrderRepository orderRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private MealRepository mealRepository;

    @InjectMocks
    private OrderService orderService;

    private Meal meal = new Meal("sarme");
    private User user = new User("Mile", "Kitic", "milekitic@yahoo.com");
    private LocalDateTime date = LocalDateTime.now();
    private RequestPostOrder crockedOrder = new RequestPostOrder();
    private String dateString = "2018-10-08";
    private RequestPostOrder requestPostOrder = new RequestPostOrder(user.getEmail(), meal.getMealName());
    private Long id = 55559L;


    private OrderFromUser orderFromUser = new OrderFromUser(id, meal, user, date);
    private List<OrderFromUser> orderList = new ArrayList<>();
    private ResponseGetOrder orderResponse = new ResponseGetOrder(orderFromUser);


    //   public ResponseGetOrder returnOrderById(Long id) throws MyResourceNotFoundException;
    @Test
    public void testReturnOrderById() throws MyResourceNotFoundException {
        //prepare

        ResponseGetOrder expected = new ResponseGetOrder(orderFromUser);


        Mockito.when(orderRepository.findById(id)).thenReturn(Optional.of(orderFromUser));
        //execute

        ResponseGetOrder actual = orderService.returnOrderById(id);

        //assert
        assertEquals(actual.getMealName(), expected.getMealName());
        assertEquals(actual.getUserFirstName(), expected.getUserFirstName());
        assertEquals(actual.getUserLastName(), expected.getUserLastName());
        assertEquals(actual.getDate(), expected.getDate());
    }

    @Test(expected = MyResourceNotFoundException.class)
    public void testReturnOrderByIdNoOrderIdPresentInDB() throws MyResourceNotFoundException {
        orderService.returnOrderById(id);
    }

    //    public List<ResponseGetOrder> returnAllOrderService() throws MyResourceNotFoundException;
    @Test
    public void testReturnAllOrderService() throws MyResourceNotFoundException {
        //prepare
        orderList.add(orderFromUser);
        List<ResponseGetOrder> expected = new ArrayList<>();
        expected.add(orderResponse);


        Mockito.when(orderRepository.findAll()).thenReturn(orderList);
        //execute

        List<ResponseGetOrder> actual = orderService.returnAllOrderService();


        //assert


        assertEquals(actual.get(0).getMealName(), expected.get(0).getMealName());
        assertEquals(actual.get(0).getUserFirstName(), expected.get(0).getUserFirstName());
        assertEquals(actual.get(0).getUserLastName(), expected.get(0).getUserLastName());
        assertEquals(actual.get(0).getDate(), expected.get(0).getDate());
    }

    //    public ResponseGetAllOrdersByEmail returnOrderByUserEmail(String email) throws MyResourceNotFoundException;
    @Test
    public void testReturnOrderByUserEmail() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        user.setEmail("saledj0777@gmail.com");
        orderList.add(orderFromUser);
        ResponseGetOrder responseGetOrder = new ResponseGetOrder(orderFromUser);
        List<ResponseGetOrder> expectedList = new ArrayList<>();
        expectedList.add(responseGetOrder);
        ResponseGetAllOrdersByEmail expected = new ResponseGetAllOrdersByEmail();
        expected.setOrderList(expectedList);
        expected.setUserName(responseGetOrder.getUserFirstName());

        Mockito.when(orderRepository.findByEmail(user.getEmail())).thenReturn(orderList);
        Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(user));

        //execute
        ResponseGetAllOrdersByEmail actual = orderService.returnOrderByUserEmail(user.getEmail());

        //assert
        assertEquals(actual.getOrderList().get(0).getMealName(), expected.getOrderList().get(0).getMealName());
        assertEquals(actual.getOrderList().get(0).getUserFirstName(), expected.getOrderList().get(0).getUserFirstName());
        assertEquals(actual.getOrderList().get(0).getUserLastName(), expected.getOrderList().get(0).getUserLastName());
        assertEquals(actual.getOrderList().get(0).getDate(), expected.getOrderList().get(0).getDate());
    }

    //    public ResponseGetAllOrders returnOrderByLocalDateTime(String date) throws MyResourceNotFoundException, DataMissingException;
    @Test
    public void testReturnOrderByDate() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        ResponseGetOrder responseGetOrder = new ResponseGetOrder("Mile", "Kitic", "sarme", "2018-10-08 10:36:24.371");
        orderList.add(orderFromUser);
        List<ResponseGetOrder> expectedList = new ArrayList<>();
        expectedList.add(responseGetOrder);
        ResponseGetAllOrders expected = new ResponseGetAllOrders();
        expected.setOrderList(expectedList);
        expected.setDatum(dateString);

        LocalDateTime[] dateObj1 = returnTwoAdjecentDays(dateString);


        Mockito.when(orderRepository.findByDate(dateObj1[0], dateObj1[1])).thenReturn(orderList);
        //execute

        ResponseGetAllOrders actual = orderService.returnOrderByDate(dateString);

        //assert
        assertEquals(actual.getOrderList().get(0).getMealName(), expected.getOrderList().get(0).getMealName());
        assertEquals(actual.getOrderList().get(0).getUserFirstName(), expected.getOrderList().get(0).getUserFirstName());
        assertEquals(actual.getOrderList().get(0).getUserLastName(), expected.getOrderList().get(0).getUserLastName());
        assertEquals(actual.getDatum(), expected.getDatum());

    }
    @Test(expected = DataMissingException.class)
    public void testReturnOrderByNotValidDateThrowsDataMissingException() throws DataMissingException, MyResourceNotFoundException {
        dateString="xmslidm";
        orderService.returnOrderByDate(dateString);

    }

    //    public ResponseGetAllOrders returnOrderByMeal(String meal) throws MyResourceNotFoundException, DataMissingException;
    @Test
    public void testReturnOrderByMeal() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        ResponseGetOrder responseGetOrder = new ResponseGetOrder(
                user.getFirstName(), user.getLastName(), meal.getMealName(), dateString);
        orderList.add(orderFromUser);
        List<ResponseGetOrder> expectedList = new ArrayList<>();
        expectedList.add(responseGetOrder);
        ResponseGetAllOrders expected = new ResponseGetAllOrders();
        expected.setOrderList(expectedList);
        expected.setDatum(dateString);

        Mockito.when(orderRepository.findByMeal(meal.getMealName())).thenReturn(orderList);
        //execute

        ResponseGetAllOrders actual = orderService.returnOrderByMeal(meal.getMealName());

        //assert
        assertEquals(actual.getOrderList().get(0).getMealName(), expected.getOrderList().get(0).getMealName());
        assertEquals(actual.getOrderList().get(0).getUserFirstName(), expected.getOrderList().get(0).getUserFirstName());
        assertEquals(actual.getOrderList().get(0).getUserLastName(), expected.getOrderList().get(0).getUserLastName());

    }


    //    public void deleteOrderById(Long id) throws MyResourceNotFoundException;
    @Test
    public void testDeleteOrderById() throws MyResourceNotFoundException {
        //prepare
        //excute
        Mockito.when(orderRepository.findById(id)).thenReturn(Optional.of(orderFromUser));
        orderService.deleteOrderById(id);

        //assert
        Mockito.verify(orderRepository, times(1)).deleteById(orderFromUser.getMealUserId());


    }

    @Test(expected = MyResourceNotFoundException.class)
    public void testDeleteOrderByIdInDBWithoutIdShouldThrowMyResourceNotFoundException() throws MyResourceNotFoundException {
        Mockito.when(orderRepository.findById(id)).thenReturn(Optional.empty());
        orderService.deleteOrderById(id);

    }

    @Test
    public void testSaveOrder() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        ResponseGetOrder expected = new ResponseGetOrder(orderFromUser);
        RequestPostOrder requestPostOrder = new RequestPostOrder(user.getEmail(), meal.getMealName());

        Mockito.when(mealRepository.findByMealName(requestPostOrder.getMealName())).thenReturn(Optional.of(meal));
        Mockito.when(userRepository.findByEmail(requestPostOrder.getUserEmail())).thenReturn(Optional.of(user));

        //execute
        ResponseGetOrder actual = orderService.saveOrder(requestPostOrder);

        //assert
        Mockito.verify(orderRepository, times(1)).save(any(OrderFromUser.class));
        verifyNoMoreInteractions(orderRepository);
        assertEquals(actual.getMealName(), expected.getMealName());
        assertEquals(actual.getUserFirstName(), expected.getUserFirstName());
        assertEquals(actual.getUserLastName(), expected.getUserLastName());
        assertEquals(actual.getDate().substring(0,actual.getDate().length()-6),
                expected.getDate().substring(0,actual.getDate().length()-6));

    }

    @Test(expected = DataMissingException.class)
    public void testSaveOrderWithoutMealShouldReturnDataMissingException() throws MyResourceNotFoundException, DataMissingException {
        //prepare

        crockedOrder.setUserEmail("saledj07@yahoo.com");

        //execute
        orderService.saveOrder(crockedOrder);

    }

    @Test(expected = DataMissingException.class)
    public void testSaveOrderWithoutUserShouldReturnDataMissingException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        crockedOrder.setMealName("burek");

        //execute
        orderService.saveOrder(crockedOrder);

    }

    @Test(expected = MyResourceNotFoundException.class)
    public void testSaveOrderWithoutMealInDBShouldReturnMyResourceNotFoundException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        //since repo is mocked no need for returning data
        // Mockito.when(mealRepository.findByMealName(requestPostOrder.getMealName())).thenReturn(Optional.empty());


        //execute
        orderService.saveOrder(requestPostOrder);

    }

    @Test(expected = MyResourceNotFoundException.class)
    public void testSaveOrderWithoutUserInDBShouldReturnMyResourceNotFoundException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        //since repo is mocked no need for returning data
        // Mockito.when(userRepository.findByEmail(requestPostOrder.getUserEmail())).thenReturn(Optional.empty());

        //execute
        orderService.saveOrder(requestPostOrder);

    }

    //    public ResponseGetOrder saveOrderWithLocalDateTime(String date, RequestPostOrder param1) throws DataMissingException, MyResourceNotFoundException, ParseException;
    @Test
    public void testSaveOrderWithDate() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        ResponseGetOrder expected = new ResponseGetOrder(
                user.getFirstName(), user.getLastName(), meal.getMealName(), dateString+"T00:00");

        RequestPostOrder requestPostOrder = new RequestPostOrder(user.getEmail(), meal.getMealName());

        Mockito.when(mealRepository.findByMealName(requestPostOrder.getMealName())).thenReturn(Optional.of(meal));
        Mockito.when(userRepository.findByEmail(requestPostOrder.getUserEmail())).thenReturn(Optional.of(user));

        //execute
        ResponseGetOrder actual = orderService.saveOrderWithDate(dateString, requestPostOrder);

        //assert
        Mockito.verify(orderRepository, times(1)).save(any(OrderFromUser.class));
        verifyNoMoreInteractions(orderRepository);
        assertEquals(actual.getMealName(), expected.getMealName());
        assertEquals(actual.getUserFirstName(), expected.getUserFirstName());
        assertEquals(actual.getUserLastName(), expected.getUserLastName());
        assertEquals(actual.getDate(), expected.getDate());
    }

    @Test(expected = DataMissingException.class)
    public void testSaveOrderWithDateWithoutMealShouldReturnDataMissingException() throws MyResourceNotFoundException, DataMissingException {
        //prepare

        crockedOrder.setUserEmail("saledj07@yahoo.com");

        //execute
        orderService.saveOrderWithDate(dateString, crockedOrder);

    }

    @Test(expected = DataMissingException.class)
    public void testSaveOrderWithDateWithoutUserShouldReturnDataMissingException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        crockedOrder.setMealName("burek");

        //execute
        orderService.saveOrderWithDate(dateString, crockedOrder);

    }


    @Test(expected = MyResourceNotFoundException.class)
    public void testSaveOrderWithDateWithoutMealInDBShouldReturnMyResourceNotFoundException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        //since repo is mocked no need for returning data
        // Mockito.when(mealRepository.findByMealName(requestPostOrder.getMealName())).thenReturn(Optional.empty());


        //execute
        orderService.saveOrderWithDate(dateString, requestPostOrder);

    }

    @Test(expected = MyResourceNotFoundException.class)
    public void testSaveOrderWithDateWithoutUserInDBShouldReturnMyResourceNotFoundException() throws MyResourceNotFoundException, DataMissingException {
        //prepare

        //execute
        orderService.saveOrderWithDate(dateString, requestPostOrder);

    }

//    public ResponseGetOrder updateOrder(Long id, RequestPostOrder requestPostOrder) throws DataMissingException, MyResourceNotFoundException;

    @Test
    public void testUpdateOrder() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        ResponseGetOrder expected = new ResponseGetOrder(
                user.getFirstName(), user.getLastName(), meal.getMealName(), dateString);
        RequestPostOrder requestPostOrder = new RequestPostOrder(user.getEmail(), meal.getMealName());

        Mockito.when(mealRepository.findByMealName(requestPostOrder.getMealName())).thenReturn(Optional.of(meal));
        Mockito.when(userRepository.findByEmail(requestPostOrder.getUserEmail())).thenReturn(Optional.of(user));
        Mockito.when(orderRepository.findById(id)).thenReturn(Optional.of(orderFromUser));

        //execute
        ResponseGetOrder actual = orderService.updateOrder(id, requestPostOrder);

        //assert
        Mockito.verify(orderRepository, times(1)).save(any(OrderFromUser.class));
        assertEquals(actual.getMealName(), expected.getMealName());
        assertEquals(actual.getUserFirstName(), expected.getUserFirstName());
        assertEquals(actual.getUserLastName(), expected.getUserLastName());

    }

    @Test(expected = DataMissingException.class)
    public void testUpdateOrderWithoutMealShouldReturnDataMissingException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        Mockito.when(orderRepository.findById(id)).thenReturn(Optional.of(orderFromUser));
        crockedOrder.setUserEmail("saledj07@yahoo.com");

        //execute
        orderService.updateOrder(id, crockedOrder);

    }

    @Test(expected = DataMissingException.class)
    public void testUpdateOrderWithoutUserShouldReturnDataMissingException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        Mockito.when(orderRepository.findById(id)).thenReturn(Optional.of(orderFromUser));
        crockedOrder.setMealName("burek");

        //execute
        orderService.updateOrder(id, crockedOrder);

    }

    @Test(expected = MyResourceNotFoundException.class)
    public void testUpdateOrderWithoutMealInDBShouldReturnMyResourceNotFoundException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        //since repo is mocked no need for returning data
        // Mockito.when(mealRepository.findByMealName(requestPostOrder.getMealName())).thenReturn(Optional.empty());


        //execute
        orderService.updateOrder(id, requestPostOrder);

    }

    @Test(expected = MyResourceNotFoundException.class)
    public void testUpdateOrderWithoutUserInDBShouldReturnMyResourceNotFoundException() throws MyResourceNotFoundException, DataMissingException {
        //prepare
        //since repo is mocked no need for returning data
        // Mockito.when(userRepository.findByEmail(requestPostOrder.getUserEmail())).thenReturn(Optional.empty());

        //execute
        orderService.updateOrder(id, requestPostOrder);

    }
}
