package com.catering.assistent.controller;

import com.catering.assistent.business.MealService;
import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.request.RequestPostMeal;
import com.catering.assistent.model.response.ResponseGetMeal;
import com.catering.assistent.repository.MealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController

@EnableJpaRepositories(basePackageClasses = MealRepository.class)

public class MealController {


    @Autowired
    private MealService mealService;

    @GetMapping("/meal/")
    public ResponseEntity<ResponseGetMeal> returnMealById(@PathParam("param1") Long param1) throws MyResourceNotFoundException {
        return ResponseEntity.ok(mealService.returnMealById(param1));
    }
    @GetMapping("/mealName/")
    public ResponseEntity<ResponseGetMeal> returnMealById(@PathParam("param2") String param2) throws MyResourceNotFoundException {
        return ResponseEntity.ok(mealService.returnMealByMealName(param2));
    }

    @GetMapping("/meals/")
    public ResponseEntity<List<ResponseGetMeal>> returnAllMeals() {
        return ResponseEntity.ok(mealService.returnAllMealsService());
    }


    @PostMapping("/meal/add/")
    public ResponseEntity<ResponseGetMeal> postNewMeal(@RequestBody RequestPostMeal param1) throws DataMissingException {
        return ResponseEntity.ok(mealService.saveMeal(param1));
    }

    @DeleteMapping("/meal/delete/")
    public void deleteMeal(@PathParam("param1") Long param1) throws MyResourceNotFoundException {
        mealService.deleteMealById(param1);
    }

    @PutMapping("meal/update/")
    public ResponseEntity<ResponseGetMeal> updateMeal(@PathParam("param1") Long id, @RequestBody @Valid RequestPostMeal param1) throws DataMissingException, MyResourceNotFoundException {
        return ResponseEntity.ok(mealService.updateMeal(id, param1));
    }
}
