package com.catering.assistent.controller;

import com.catering.assistent.business.OrderService;
import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.request.RequestPostOrder;
import com.catering.assistent.model.response.ResponseGetAllOrders;
import com.catering.assistent.model.response.ResponseGetAllOrdersByEmail;
import com.catering.assistent.model.response.ResponseGetOrder;
import com.catering.assistent.repository.OrderRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import java.util.List;

@RestController

@EnableJpaRepositories(basePackageClasses = OrderRepository.class)
public class OrderController {

    @Autowired
    private OrderService orderService;

    @GetMapping("/order/")
    public ResponseEntity<ResponseGetOrder> returnOrderById(@PathParam("param1") Long param1) throws MyResourceNotFoundException {
        return ResponseEntity.ok(orderService.returnOrderById(param1));
    }

    @GetMapping("/orders/")
    public ResponseEntity<List<ResponseGetOrder>> returnAllOrders() throws MyResourceNotFoundException {
        return ResponseEntity.ok(orderService.returnAllOrderService());
    }

    @GetMapping("/ordersByUserEmail/")
    public ResponseEntity<ResponseGetAllOrdersByEmail> returnAllOrdersByEmail(@PathParam("email") String email) throws MyResourceNotFoundException, DataMissingException {
        return ResponseEntity.ok(orderService.returnOrderByUserEmail(email));
    }

    @GetMapping("/ordersByDate/")
    public ResponseEntity<ResponseGetAllOrders> returnAllOrdersByDate(@PathParam("date")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String date) throws MyResourceNotFoundException, DataMissingException {
        return ResponseEntity.ok(orderService.returnOrderByDate(date));

    }
    @GetMapping("/ordersByMeal/")
    public ResponseEntity<ResponseGetAllOrders> returnAllOrdersByMeal(@PathParam("meal") String meal) throws MyResourceNotFoundException, DataMissingException {
        return ResponseEntity.ok(orderService.returnOrderByMeal(meal));

    }
    @PostMapping("/order/add/")
    public ResponseEntity<ResponseGetOrder> postNewOrder(@RequestBody @Valid RequestPostOrder param1) throws DataMissingException, MyResourceNotFoundException {
        return ResponseEntity.ok(orderService.saveOrder(param1));
    }
    @PostMapping("/order/addWithDate/")
    public ResponseEntity<ResponseGetOrder> postNewOrderWithDate(@PathParam("date")@DateTimeFormat(iso = DateTimeFormat.ISO.DATE) String date, @RequestBody @Valid RequestPostOrder param1) throws DataMissingException, MyResourceNotFoundException {
        return ResponseEntity.ok(orderService.saveOrderWithDate(date,param1));
    }

    @DeleteMapping("/order/delete/")
    public void deleteOrder(@PathParam("param1") Long param1) throws MyResourceNotFoundException {
        orderService.deleteOrderById(param1);
    }

    @PutMapping("order/update/")
    public ResponseEntity<ResponseGetOrder> updateOrder(@PathParam("param1") Long id, @RequestBody @Valid RequestPostOrder param1) throws DataMissingException, MyResourceNotFoundException {
        return ResponseEntity.ok(orderService.updateOrder(id, param1));
    }
}
