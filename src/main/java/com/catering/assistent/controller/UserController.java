package com.catering.assistent.controller;


import com.catering.assistent.business.UserService;
import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.EmailExistentException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.User;
import com.catering.assistent.model.request.RequestPostUser;
import com.catering.assistent.model.response.ResponseGetUser;
import com.catering.assistent.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import org.springframework.web.bind.annotation.*;
import java.util.List;


@RestController

@EnableJpaRepositories(basePackageClasses = UserRepository.class)

public class UserController {


    @Autowired
    private UserService userService;

    @GetMapping("/user/")
    public ResponseEntity<ResponseGetUser> returnUserByIdController(@PathParam("id") Long id) throws MyResourceNotFoundException {
        return ResponseEntity.ok(userService.returnUserById(id));
    }

    @GetMapping("/userByEmail/")
    public ResponseEntity<ResponseGetUser> returnUserByEmailController(@PathParam("email") String email) throws MyResourceNotFoundException, DataMissingException {
        return ResponseEntity.ok(userService.returnUserByEmail(email));
    }

    @GetMapping("/users/")
    public ResponseEntity<List<ResponseGetUser>> returnAllUsers() {
    return ResponseEntity.ok(userService.returnAllUsersService());
    }


    @PostMapping("/user/add/")
    public ResponseEntity<ResponseGetUser> postNewUser(@RequestBody @Valid RequestPostUser param1) throws DataMissingException, EmailExistentException {
        return ResponseEntity.ok(userService.saveUser(param1));
    }

    @DeleteMapping("/user/delete/")
    public void deleteUser(@PathParam("param1") Long param1) throws MyResourceNotFoundException {
        userService.deleteUserById(param1);
    }
    @PutMapping("user/update/")
    public ResponseEntity<ResponseGetUser> updateUser(@PathParam("param1") Long id, @RequestBody @Valid RequestPostUser param1) throws DataMissingException, MyResourceNotFoundException {
        return ResponseEntity.ok(userService.updateUser(id, param1));
    }
}
