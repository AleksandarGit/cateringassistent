package com.catering.assistent.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import com.catering.assistent.model.OrderFromUser;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<OrderFromUser, Long> {
    @Query("SELECT o FROM OrderFromUser o WHERE o.user.id = (SELECT id FROM User u where email=?1)")
    public List<OrderFromUser> findByEmail(String email);

    @Query("SELECT o FROM OrderFromUser o WHERE o.date >= ?1 AND o.date < ?2")
    public List<OrderFromUser> findByDate(LocalDateTime datum1, LocalDateTime datum2);

    @Query("SELECT o FROM OrderFromUser o WHERE o.meal.mealName = ?1")
    public List<OrderFromUser> findByMeal(String meal);

}
