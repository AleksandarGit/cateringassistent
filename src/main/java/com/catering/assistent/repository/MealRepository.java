package com.catering.assistent.repository;

import com.catering.assistent.model.Meal;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MealRepository extends CrudRepository<Meal, Long> {
    Optional<Meal> findByMealName(String mealName);
}
