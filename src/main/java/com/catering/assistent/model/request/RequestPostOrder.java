package com.catering.assistent.model.request;


import javax.validation.constraints.NotNull;

public class RequestPostOrder {
    @NotNull
    private String userEmail;
    @NotNull
    private String mealName;


    public RequestPostOrder() {
    }

    public RequestPostOrder(String userEmail, String mealName) {
        this.userEmail = userEmail;
        this.mealName = mealName;

    }

    public RequestPostOrder(String userEmail, String mealName, String date) {
        this.userEmail = userEmail;
        this.mealName = mealName;

    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }


    @Override
    public String toString() {
        return "RequestPostOrder{" +
                "userEmail='" + userEmail + '\'' +
                ", mealName='" + mealName + '\'' +
                '}';
    }
}
