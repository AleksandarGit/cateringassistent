package com.catering.assistent.model.request;

import javax.validation.constraints.NotNull;
import java.util.Objects;

public class RequestPostMeal {

    @NotNull
    private String meal;

    public RequestPostMeal() {
    }

    public RequestPostMeal(String meal) {
        this.meal = meal;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestPostMeal that = (RequestPostMeal) o;
        return Objects.equals(meal, that.meal);
    }

    @Override
    public int hashCode() {
        return Objects.hash(meal);
    }

    @Override
    public String toString() {
        return "RequestPostMeal{" +
                "meal=" + meal +
                '}';
    }
}
