package com.catering.assistent.model;


import javax.persistence.*;


import com.fasterxml.jackson.annotation.JsonBackReference;

import java.time.LocalDateTime;
import java.util.Objects;

@Entity(name = "OrderFromUser")
@Table(name = "orderFromUser")
public class OrderFromUser {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long mealUserId;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    private Meal meal;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @Column(name = "date")
    private LocalDateTime date;

    public OrderFromUser() {
    }

    public OrderFromUser(Meal meal, User user) {
        this.meal = meal;
        this.user = user;

    }
    public OrderFromUser(Meal meal, User user, LocalDateTime date) {
        this.meal = meal;
        this.user = user;
        this.date = date;

    }

    public OrderFromUser(Long mealUserId, Meal meal, User user, LocalDateTime date) {
        this.mealUserId = mealUserId;
        this.meal = meal;
        this.user = user;
        this.date = date;
    }

    public Long getMealUserId() {
        return mealUserId;
    }

    public void setMealUserId(Long mealUserId) {
        this.mealUserId = mealUserId;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getLocalDateTime() {
        return date;
    }

    public void setLocalDateTime(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderFromUser that = (OrderFromUser) o;
        return Objects.equals(mealUserId, that.mealUserId) &&
                Objects.equals(meal, that.meal) &&
                Objects.equals(user, that.user) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mealUserId, meal, user, date);
    }

    @Override
    public String toString() {
        return "OrderFromUser{" +
                "mealUserId=" + mealUserId +
                ", meal=" + meal +
                ", user=" + user +
                ", date=" + date +
                '}';
    }
}