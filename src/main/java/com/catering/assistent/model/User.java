package com.catering.assistent.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity(name = "User")
@Table(name = "users")

public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userId")
    private long id;

    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    @Email
    private String email;

    @OneToMany(
            mappedBy = "user",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @JsonManagedReference
    private List<OrderFromUser> orderFromUsers = new ArrayList<>();

    public void addMeal(Meal meal) {
        OrderFromUser orderFromUser = new OrderFromUser(meal, this);
        orderFromUsers.add(orderFromUser);
        meal.getOrders().add(orderFromUser);
    }

    public void removeMeal(Meal meal) {
        for (Iterator<OrderFromUser> iterator = orderFromUsers.iterator();
             iterator.hasNext(); ) {
            OrderFromUser orderFromUser = iterator.next();

            if (orderFromUser.getUser().equals(this) &&
                    orderFromUser.getMeal().equals(meal)) {
                iterator.remove();
                orderFromUser.getMeal().getOrders().remove(orderFromUser);
                orderFromUser.setUser(null);
                orderFromUser.setMeal(null);
            }
        }
    }

    public User() {
    }

    public User(String firstName, String lastName, @Email String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }
    public User(long id, String firstName, String lastName, @Email String email) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<OrderFromUser> getOrders() {
        return orderFromUsers;
    }

    public void setOrders(List<OrderFromUser> orderFromUsers) {
        this.orderFromUsers = orderFromUsers;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
