package com.catering.assistent.model.response;

import com.catering.assistent.model.OrderFromUser;
import com.catering.assistent.model.User;


import java.util.List;
import java.util.Objects;

public class ResponseGetAllOrdersByEmail {
    private String userName;
    private List<ResponseGetOrder> orderList;

    public ResponseGetAllOrdersByEmail() {
    }

    public ResponseGetAllOrdersByEmail(String userName, List<ResponseGetOrder> orderList) {
        this.userName = userName;
        this.orderList = orderList;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public List<ResponseGetOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<ResponseGetOrder> orderList) {
        this.orderList = orderList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseGetAllOrdersByEmail that = (ResponseGetAllOrdersByEmail) o;
        return Objects.equals(userName, that.userName) &&
                Objects.equals(orderList, that.orderList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userName, orderList);
    }

    @Override
    public String toString() {
        return "ResponseGetAllOrdersByEmail{" +
                "User name and email=" + userName +
                ", List of orders=" + orderList +
                '}';
    }
}
