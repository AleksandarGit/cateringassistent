package com.catering.assistent.model.response;

import com.catering.assistent.model.OrderFromUser;


import java.util.List;
import java.util.Objects;

public class ResponseGetAllOrders {
    private String datum;
    private List<ResponseGetOrder> orderList;

    public ResponseGetAllOrders() {
    }

    public ResponseGetAllOrders(String datum, List<ResponseGetOrder> orderList) {
        this.datum = datum;
        this.orderList = orderList;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public List<ResponseGetOrder> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<ResponseGetOrder> orderList) {
        this.orderList = orderList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseGetAllOrders that = (ResponseGetAllOrders) o;
        return Objects.equals(datum, that.datum) &&
                Objects.equals(orderList, that.orderList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(datum, orderList);
    }

    @Override
    public String toString() {
        return "ResponseGetAllOrders{" +
                "Date=" + datum +
                ", List of orders=" + orderList +
                '}';
    }
}
