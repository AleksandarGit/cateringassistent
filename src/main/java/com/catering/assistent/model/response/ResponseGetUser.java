package com.catering.assistent.model.response;

import com.catering.assistent.model.User;

import java.util.Objects;

public class ResponseGetUser {
    private String userFirstName;
    private String userLastName;
    private String userEmail;

    public ResponseGetUser() {
    }

    public ResponseGetUser(User user) {
        this.userFirstName = user.getFirstName();
        this.userLastName = user.getLastName();
        this.userEmail = user.getEmail();
    }
    public ResponseGetUser(String userFirstName, String userLastName, String userEmail) {
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.userEmail = userEmail;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseGetUser that = (ResponseGetUser) o;
        return Objects.equals(userFirstName, that.userFirstName) &&
                Objects.equals(userLastName, that.userLastName) &&
                Objects.equals(userEmail, that.userEmail);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userFirstName, userLastName, userEmail);
    }

    @Override
    public String toString() {
        return "ResponseGetUser{" +
                "First name='" + userFirstName + '\'' +
                ", Last name='" + userLastName + '\'' +
                ", User email='" + userEmail + '\'' +
                '}';
    }
}
