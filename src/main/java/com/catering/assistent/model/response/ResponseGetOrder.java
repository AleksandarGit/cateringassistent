package com.catering.assistent.model.response;

import com.catering.assistent.model.OrderFromUser;

import java.util.Date;
import java.util.Objects;

public class ResponseGetOrder {
    private String userFirstName;
    private String userLastName;
    private String mealName;
    private String date;

    public ResponseGetOrder() {
    }

    public ResponseGetOrder(OrderFromUser orderFromUser) {
        this.userFirstName = orderFromUser.getUser().getFirstName();
        this.userLastName = orderFromUser.getUser().getLastName();
        this.mealName = orderFromUser.getMeal().getMealName();
        this.date = orderFromUser.getLocalDateTime().toString();
    }

    public ResponseGetOrder(String userFirstName, String userLastName, String mealName, String date) {
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.mealName = mealName;
        this.date = date;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseGetOrder that = (ResponseGetOrder) o;
        return Objects.equals(userFirstName, that.userFirstName) &&
                Objects.equals(userLastName, that.userLastName) &&
                Objects.equals(mealName, that.mealName) &&
                Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userFirstName, userLastName, mealName, date);
    }

    @Override
    public String toString() {
        return "ResponseGetOrder{" +
                "First name='" + userFirstName + '\'' +
                ", Last name='" + userLastName + '\'' +
                ", Meal name='" + mealName + '\'' +
                ", Date='" + date + '\'' +
                '}';
    }
}
