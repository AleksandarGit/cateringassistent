package com.catering.assistent.model.response;

import com.catering.assistent.model.Meal;

import java.util.Objects;

public class ResponseGetMeal {
    private String mealName;

    public ResponseGetMeal() {
    }

    public ResponseGetMeal(String mealName) {
        this.mealName = mealName;
    }
    public ResponseGetMeal(Meal meal) {
        this.mealName = meal.getMealName();
    }


    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResponseGetMeal that = (ResponseGetMeal) o;
        return Objects.equals(mealName, that.mealName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mealName);
    }

    @Override
    public String toString() {
        return "ResponseGetMeal{" +
                "Meal name='" + mealName + '\'' +
                '}';
    }
}
