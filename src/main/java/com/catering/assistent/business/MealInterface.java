package com.catering.assistent.business;

import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.request.RequestPostMeal;
import com.catering.assistent.model.response.ResponseGetMeal;

import java.util.List;

public interface MealInterface {
    List<ResponseGetMeal> returnAllMealsService();

    ResponseGetMeal returnMealById(Long id) throws MyResourceNotFoundException;

    void deleteMealById(Long id) throws MyResourceNotFoundException;

    ResponseGetMeal saveMeal(RequestPostMeal requestPostMeal) throws DataMissingException;

    ResponseGetMeal updateMeal(Long id, RequestPostMeal requestPostMeal) throws DataMissingException, MyResourceNotFoundException;

    ResponseGetMeal returnMealByMealName(String mealName) throws MyResourceNotFoundException;

}
