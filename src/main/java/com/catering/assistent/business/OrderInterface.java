package com.catering.assistent.business;


import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.request.RequestPostOrder;
import com.catering.assistent.model.response.*;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public interface OrderInterface {


    ResponseGetOrder returnOrderById(Long id) throws MyResourceNotFoundException;

    List<ResponseGetOrder> returnAllOrderService() throws MyResourceNotFoundException;

    void deleteOrderById(Long id) throws MyResourceNotFoundException;

    ResponseGetOrder saveOrder(RequestPostOrder requestPostOrder) throws DataMissingException, MyResourceNotFoundException;

    ResponseGetOrder updateOrder(Long id, RequestPostOrder requestPostOrder) throws DataMissingException, MyResourceNotFoundException;

    ResponseGetAllOrdersByEmail returnOrderByUserEmail(String email) throws MyResourceNotFoundException, DataMissingException;

    ResponseGetAllOrders returnOrderByDate(String date) throws MyResourceNotFoundException, DataMissingException;

    ResponseGetAllOrders returnOrderByMeal(String meal) throws MyResourceNotFoundException, DataMissingException;

    ResponseGetOrder saveOrderWithDate(String date, RequestPostOrder param1) throws DataMissingException, MyResourceNotFoundException, ParseException;
}