package com.catering.assistent.business;

import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.EmailExistentException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.request.RequestPostUser;
import com.catering.assistent.model.response.ResponseGetUser;

import java.util.List;

public interface UserInterface {
    List<ResponseGetUser> returnAllUsersService();

    ResponseGetUser returnUserById(Long id) throws MyResourceNotFoundException;

    void deleteUserById(Long id) throws MyResourceNotFoundException;

    ResponseGetUser saveUser(RequestPostUser requestPostUser) throws DataMissingException, EmailExistentException;

    ResponseGetUser updateUser(Long id, RequestPostUser requestPostUser) throws DataMissingException,MyResourceNotFoundException;
}
