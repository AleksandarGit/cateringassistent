package com.catering.assistent.business;

import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.EmailExistentException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.User;
import com.catering.assistent.model.request.RequestPostUser;
import com.catering.assistent.model.response.ResponseGetUser;
import com.catering.assistent.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.catering.assistent.util.EmailValidation.validateEmailApache;

@Service
@Transactional
public class UserService implements UserInterface {
    @Autowired
    private UserRepository userRepository;


    @Override
    public ResponseGetUser returnUserById(Long id) throws MyResourceNotFoundException {
        Optional<User> userById = userRepository.findById(id);
        if (!userById.isPresent()) {
            throw new MyResourceNotFoundException("No user with that id present.");
        }
        ResponseGetUser responseGetUserById = new ResponseGetUser();
        responseGetUserById.setUserFirstName(userById.get().getFirstName());
        responseGetUserById.setUserLastName(userById.get().getLastName());
        responseGetUserById.setUserEmail(userById.get().getEmail());
        return responseGetUserById;
    }
    public ResponseGetUser returnUserByEmail(String email) throws MyResourceNotFoundException, DataMissingException {
        if (email == null) {
            throw new DataMissingException("No email entered.");
        }
        validateEmailApache(email);
        Optional<User> userByEmail = userRepository.findByEmail(email);
        if (!userByEmail.isPresent()) {
            throw new MyResourceNotFoundException("No user with that email present.");
        }
        ResponseGetUser responseGetUserByEmail = new ResponseGetUser();
        responseGetUserByEmail.setUserFirstName(userByEmail.get().getFirstName());
        responseGetUserByEmail.setUserLastName(userByEmail.get().getLastName());
        responseGetUserByEmail.setUserEmail(userByEmail.get().getEmail());
        return responseGetUserByEmail;
    }

    @Override
    public void deleteUserById(Long id) throws MyResourceNotFoundException {
        Optional<User> userById = userRepository.findById(id);
        if (!userById.isPresent()) {
            throw new MyResourceNotFoundException("No user with that id present.");
        }
        userRepository.deleteById(id);
    }

    @Override
    public ResponseGetUser saveUser(RequestPostUser requestPostUser) throws DataMissingException, EmailExistentException {
        if ((requestPostUser.getUserFirstName() == null)
                || (requestPostUser.getUserLastName() == null)
                || (requestPostUser.getUserEmail() == null)) {
            throw new DataMissingException("Some data are missing in the form!");

        }
        if (userRepository.findByEmail(requestPostUser.getUserEmail()).isPresent()){
            throw new EmailExistentException("Email exist in form!");
        }
        User user = new User();
        user.setFirstName(requestPostUser.getUserFirstName());
        user.setLastName(requestPostUser.getUserLastName());
        user.setEmail(requestPostUser.getUserEmail());


        userRepository.save(user);


        ResponseGetUser responseGetUser = new ResponseGetUser();
        responseGetUser.setUserEmail(user.getEmail());
        responseGetUser.setUserFirstName(user.getFirstName());
        responseGetUser.setUserLastName(user.getLastName());

        return responseGetUser;
    }

    @Override
    public ResponseGetUser updateUser(Long id, RequestPostUser requestPostUser) throws DataMissingException, MyResourceNotFoundException {

        Optional<User> userById = userRepository.findById(id);
        if (!userById.isPresent()) {
            throw new MyResourceNotFoundException("No user with that id present.");
        }

        if ((requestPostUser.getUserFirstName() == null)
                || (requestPostUser.getUserLastName() == null)
                || (requestPostUser.getUserEmail() == null)) {
            throw new DataMissingException("Some data are missing in the form!");

        }

        userById.get().setEmail(requestPostUser.getUserEmail());
        userById.get().setFirstName(requestPostUser.getUserFirstName());
        userById.get().setLastName(requestPostUser.getUserLastName());


        ResponseGetUser responseGetUser = new ResponseGetUser();
        responseGetUser.setUserEmail(userById.get().getEmail());
        responseGetUser.setUserFirstName(userById.get().getFirstName());
        responseGetUser.setUserLastName(userById.get().getLastName());


        return responseGetUser;
    }

    @Override
    public List<ResponseGetUser> returnAllUsersService() {

        List<User> lista = (List<User>) userRepository.findAll();
        List<ResponseGetUser> responseUserList = new ArrayList<>();

        lista.forEach(item -> responseUserList.add(new ResponseGetUser(item)));

        return responseUserList;
    }

}

