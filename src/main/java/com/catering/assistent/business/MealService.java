package com.catering.assistent.business;

import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.request.RequestPostMeal;
import com.catering.assistent.model.response.ResponseGetMeal;
import com.catering.assistent.repository.MealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class MealService implements MealInterface {

    @Autowired
    private MealRepository mealRepository;

    @Override
    public List<ResponseGetMeal> returnAllMealsService() {
        List<Meal> lista = (List<Meal>) mealRepository.findAll();
        List<ResponseGetMeal> responseMealList = new ArrayList<>();

        lista.forEach(item -> responseMealList.add(new ResponseGetMeal(item)));

        return responseMealList;
    }

    @Override
    public ResponseGetMeal returnMealById(Long id) throws MyResourceNotFoundException {
        Optional<Meal> mealById = mealRepository.findById(id);
        if (!mealById.isPresent()) {
            throw new MyResourceNotFoundException("No meal with that id present.");
        }
        ResponseGetMeal responseGetMealById = new ResponseGetMeal();
        responseGetMealById.setMealName(mealById.get().getMealName());
        return responseGetMealById;
    }

    public ResponseGetMeal returnMealByMealName(String mealName) throws MyResourceNotFoundException {
        Optional<Meal> mealByMealName = mealRepository.findByMealName(mealName);
        if (!mealByMealName.isPresent()) {
            throw new MyResourceNotFoundException("No meal with that name present.");
        }
        ResponseGetMeal responseGetMealById = new ResponseGetMeal();
        responseGetMealById.setMealName(mealByMealName.get().getMealName());
        return responseGetMealById;
    }

    @Override
    public void deleteMealById(Long id) throws MyResourceNotFoundException {
        Optional<Meal> mealById = mealRepository.findById(id);
        if (!mealById.isPresent()) {
            throw new MyResourceNotFoundException("No meal with that id present.");
        }
        mealRepository.deleteById(id);

    }

    @Override
    public ResponseGetMeal saveMeal(RequestPostMeal requestPostMeal) throws DataMissingException {

        if ((requestPostMeal.getMeal() == null)) {
            throw new DataMissingException("Some data are missing in the form!");
        }

        Meal request = new Meal(requestPostMeal.getMeal());
        mealRepository.save(request);


        ResponseGetMeal responseGetMeal = new ResponseGetMeal();
        responseGetMeal.setMealName(request.getMealName());
        return responseGetMeal;
    }

    @Override
    public ResponseGetMeal updateMeal(Long id, RequestPostMeal requestPostMeal) throws DataMissingException, MyResourceNotFoundException {
        if (requestPostMeal.getMeal() == null) {
            throw new MyResourceNotFoundException("No no meal available.");
        }
        Optional<Meal> mealById = mealRepository.findById(id);
        if (!mealById.isPresent()) {
            throw new MyResourceNotFoundException("No user with that id present.");
        }
        if ((requestPostMeal.getMeal() == null)) {
            throw new DataMissingException("Some data are missing in the form!");

        }
        mealById.get().setMealName(requestPostMeal.getMeal());


        ResponseGetMeal responseGetMeal = new ResponseGetMeal();
        responseGetMeal.setMealName(mealById.get().getMealName());
        return responseGetMeal;
    }
}
