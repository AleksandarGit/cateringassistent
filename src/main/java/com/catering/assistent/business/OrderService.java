package com.catering.assistent.business;

import com.catering.assistent.exception.DataMissingException;
import com.catering.assistent.exception.MyResourceNotFoundException;
import com.catering.assistent.model.Meal;
import com.catering.assistent.model.OrderFromUser;
import com.catering.assistent.model.User;
import com.catering.assistent.model.request.RequestPostOrder;
import com.catering.assistent.model.response.*;
import com.catering.assistent.repository.MealRepository;
import com.catering.assistent.repository.OrderRepository;
import com.catering.assistent.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.catering.assistent.util.EmailValidation.validateEmailApache;
import static com.catering.assistent.util.StringDateConverter.returnTwoAdjecentDays;

@Service
@Transactional
public class OrderService implements OrderInterface {

    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private MealRepository mealRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseGetOrder returnOrderById(Long id) throws MyResourceNotFoundException {
        Optional<com.catering.assistent.model.OrderFromUser> orderById = orderRepository.findById(id);
        if (!orderById.isPresent()) {
            throw new MyResourceNotFoundException("No order with that id present.");
        }
        OrderFromUser orderFromUser = orderById.get();
        return new ResponseGetOrder(orderFromUser);
    }

    @Override
    public List<ResponseGetOrder> returnAllOrderService() throws MyResourceNotFoundException {
        List<OrderFromUser> lista = (List<OrderFromUser>) orderRepository.findAll();
        if (lista.isEmpty()) {
            throw new MyResourceNotFoundException("No orders found.");
        }
        List<ResponseGetOrder> responseOrderList = new ArrayList<>();

        lista.forEach(item -> responseOrderList.add(new ResponseGetOrder(item)));

        return responseOrderList;

    }

    @Override
    public void deleteOrderById(Long id) throws MyResourceNotFoundException {
        Optional<com.catering.assistent.model.OrderFromUser> orderById = orderRepository.findById(id);
        if (!orderById.isPresent()) {
            throw new MyResourceNotFoundException("No order with that id present.");
        }
        orderRepository.deleteById(id);
    }

    @Override
    public ResponseGetOrder saveOrder(RequestPostOrder requestPostOrder)
            throws DataMissingException, MyResourceNotFoundException {
        if ((requestPostOrder.getMealName() == null) || (requestPostOrder.getUserEmail() == null)) {
            throw new DataMissingException("Some data are missing in the form!");
        }

        Optional<Meal> opMeal = mealRepository.findByMealName(requestPostOrder.getMealName());
        if (!opMeal.isPresent()) {
            throw new MyResourceNotFoundException("No meal with that name present.");
        }
        Optional<User> opUser = userRepository.findByEmail(requestPostOrder.getUserEmail());
        if (!opUser.isPresent()) {
            throw new MyResourceNotFoundException("No user with that email present.");
        }

        OrderFromUser orderFromUser = new OrderFromUser();
        orderFromUser.setMeal(opMeal.get());
        orderFromUser.setUser(opUser.get());
        orderFromUser.setLocalDateTime(LocalDateTime.now());

        orderRepository.save(orderFromUser);
        return new ResponseGetOrder(orderFromUser);
    }

    @Override
    public ResponseGetOrder updateOrder(Long id, RequestPostOrder requestPostOrder)
            throws DataMissingException, MyResourceNotFoundException {

        Optional<OrderFromUser> opOrderFromUser = orderRepository.findById(id);

        if (!opOrderFromUser.isPresent()) {
            throw new MyResourceNotFoundException("No order with that id present.");
        }

        if ((requestPostOrder.getMealName() == null) || (requestPostOrder.getUserEmail() == null)) {
            throw new DataMissingException("Some data are missing in the form!");
        }

        Optional<Meal> opMeal = mealRepository.findByMealName(requestPostOrder.getMealName());
        if (!opMeal.isPresent()) {
            throw new MyResourceNotFoundException("No meal with that name present.");
        }

        Optional<User> opUser = userRepository.findByEmail(requestPostOrder.getUserEmail());
        if (!opUser.isPresent()) {
            throw new MyResourceNotFoundException("No user with that email present.");
        }
        OrderFromUser orderFromUser = opOrderFromUser.get();
        orderFromUser.setMeal(opMeal.get());
        orderFromUser.setUser(opUser.get());
        orderFromUser.setLocalDateTime(LocalDateTime.now());

        orderRepository.save(orderFromUser);
        return new ResponseGetOrder(orderFromUser);
    }

    @Override
    public ResponseGetAllOrdersByEmail returnOrderByUserEmail(String email) throws MyResourceNotFoundException
            , DataMissingException {
        if (email == null) {
            throw new DataMissingException("No email entered.");
        }
        validateEmailApache(email);
        List<OrderFromUser> orderByEmail = orderRepository.findByEmail(email);
        if (orderByEmail.isEmpty()) {
            throw new MyResourceNotFoundException("No orders for that user.");
        }

        List<ResponseGetOrder> responseOrderList = new ArrayList<>();

        orderByEmail.forEach(item -> responseOrderList.add(new ResponseGetOrder(item)));

        User userByEmail = userRepository.findByEmail(email).get();
        ResponseGetAllOrdersByEmail responseGetAllOrdersByEmail = new ResponseGetAllOrdersByEmail();
        responseGetAllOrdersByEmail.setOrderList(responseOrderList);
        responseGetAllOrdersByEmail.setUserName(userByEmail.getFirstName() + " " + userByEmail.getLastName() + " " +
                userByEmail.getEmail());

        return responseGetAllOrdersByEmail;
    }


    @Override
    public ResponseGetAllOrders returnOrderByDate(String dateString) throws MyResourceNotFoundException, DataMissingException {

        if (dateString == null) {
            throw new DataMissingException("You did not enter the date for your query!");
        }
        LocalDateTime[] dateTimeObjs = returnTwoAdjecentDays(dateString);

        List<OrderFromUser> orderByDate = orderRepository.findByDate(dateTimeObjs[0], dateTimeObjs[1]);

        List<ResponseGetOrder> responseOrderList = new ArrayList<>();

        orderByDate.forEach(item -> responseOrderList.add(new ResponseGetOrder(item)));

        if (orderByDate.isEmpty()) {
            throw new MyResourceNotFoundException("No orders for that date.");
        }


        ResponseGetAllOrders responseGetAllOrdersByDate = new ResponseGetAllOrders();
        responseGetAllOrdersByDate.setDatum(dateString);
        responseGetAllOrdersByDate.setOrderList(responseOrderList);
        return responseGetAllOrdersByDate;
    }

    @Override
    public ResponseGetAllOrders returnOrderByMeal(String meal) throws MyResourceNotFoundException, DataMissingException {
        if (meal == null) {
            throw new DataMissingException("Nothing in meal box.");
        }
        List<OrderFromUser> orderByMeal = orderRepository.findByMeal(meal);
        List<ResponseGetOrder> responseOrderList = new ArrayList<>();

        orderByMeal.forEach(item -> responseOrderList.add(new ResponseGetOrder(item)));
        if (orderByMeal.isEmpty()) {
            throw new MyResourceNotFoundException("No orders with that meal.");
        }

        ResponseGetAllOrders responseGetAllOrdersByMeal = new ResponseGetAllOrders();
        responseGetAllOrdersByMeal.setOrderList(responseOrderList);
        return responseGetAllOrdersByMeal;

    }

    @Override
    public ResponseGetOrder saveOrderWithDate(String date, RequestPostOrder requestPostOrder)
            throws DataMissingException, MyResourceNotFoundException {
        if ((requestPostOrder.getMealName() == null) || (requestPostOrder.getUserEmail() == null)) {
            throw new DataMissingException("Some data are missing in the form!");
        }
        Optional<Meal> opMeal = mealRepository.findByMealName(requestPostOrder.getMealName());
        if (!opMeal.isPresent()) {
            throw new MyResourceNotFoundException("No meal with that name present.");
        }
        Optional<User> opUser = userRepository.findByEmail(requestPostOrder.getUserEmail());
        if (!opUser.isPresent()) {
            throw new MyResourceNotFoundException("No user with that email present.");
        }

        LocalDateTime dateObj1 = returnTwoAdjecentDays(date)[0];

        OrderFromUser orderFromUser = new OrderFromUser();
        orderFromUser.setMeal(opMeal.get());
        orderFromUser.setUser(opUser.get());
        orderFromUser.setLocalDateTime(dateObj1);

        orderRepository.save(orderFromUser);
        ResponseGetOrder responseGetOrder = new ResponseGetOrder();
        responseGetOrder.setUserLastName(orderFromUser.getUser().getLastName());
        responseGetOrder.setUserFirstName(orderFromUser.getUser().getFirstName());
        responseGetOrder.setMealName(orderFromUser.getMeal().getMealName());
        responseGetOrder.setDate(orderFromUser.getLocalDateTime().toString());

        return responseGetOrder;

    }

}
