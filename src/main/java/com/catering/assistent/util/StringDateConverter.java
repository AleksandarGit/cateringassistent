package com.catering.assistent.util;

import com.catering.assistent.exception.DataMissingException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringDateConverter {

    public static LocalDateTime[] returnTwoAdjecentDays(String stringDate) throws DataMissingException {

        validateDate(stringDate);
        LocalDateTime dateObj1 = LocalDate.parse(stringDate).atStartOfDay();
        LocalDateTime dateObj2 = dateObj1.plusDays(1L);

        LocalDateTime [] days = {dateObj1, dateObj2};
        return days;
    }
    private static Pattern DATE_PATTERN = Pattern.compile(
            "^((2000|2400|2800|(19|2[0-9](0[48]|[2468][048]|[13579][26])))-02-29)$"
                    + "|^(((19|2[0-9])[0-9]{2})-02-(0[1-9]|1[0-9]|2[0-8]))$"
                    + "|^(((19|2[0-9])[0-9]{2})-(0[13578]|10|12)-(0[1-9]|[12][0-9]|3[01]))$"
                    + "|^(((19|2[0-9])[0-9]{2})-(0[469]|11)-(0[1-9]|[12][0-9]|30))$");


    public static void validateDate(String stringDate) throws DataMissingException {
        Matcher matcher = DATE_PATTERN.matcher(stringDate);

        if (!matcher.find()) {
            throw new DataMissingException("Not a valid date! Enter Date in format: YYYY-MM-DD");
        }
    }
}
