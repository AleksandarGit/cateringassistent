package com.catering.assistent.util;


import com.catering.assistent.exception.DataMissingException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static void validateEmailApache(String emailStr) throws DataMissingException {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);

        if (!matcher.find()) {
            throw new DataMissingException("Not a valid email address!");
        }
    }
}
