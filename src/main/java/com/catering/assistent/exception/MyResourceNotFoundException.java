package com.catering.assistent.exception;

public class MyResourceNotFoundException extends Exception {

    public MyResourceNotFoundException(final String message) {
        super(message);
    }
}

