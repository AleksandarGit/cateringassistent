package com.catering.assistent.exception;

public class EmailExistentException extends Exception{

    public EmailExistentException(final String message) {
        super(message);
    }
}
