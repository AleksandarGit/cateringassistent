package com.catering.assistent.exception;

public class DataMissingException extends Exception {

    public DataMissingException(final String message) {
        super(message);
    }
}
